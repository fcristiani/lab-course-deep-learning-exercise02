from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

# Imports
import numpy as np
import tensorflow as tf
import time

tf.logging.set_verbosity(tf.logging.INFO)

LEARNING_RATE = 0.1

def cnn_model_fn(features, labels, mode):
	with tf.device('/cpu:0'):
	# with tf.device('/device:GPU:0'):
		"""Model function for CNN."""
		# Input Layer
		input_layer = tf.reshape(features["x"], [-1, 28, 28, 1])

		number_of_filters = 32

		# Convolutional Layer #1
		conv1 = tf.layers.conv2d(
			inputs=input_layer,
			filters=number_of_filters,
			kernel_size=[3, 3],
			padding="same",
			activation=tf.nn.relu
		)

		# Pooling Layer #1
		pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=1)

		# Convolutional Layer #2 and Pooling Layer #2
		conv2 = tf.layers.conv2d(
			inputs=pool1,
			filters=number_of_filters,
			kernel_size=[3, 3],
			padding="same",
			activation=tf.nn.relu
		)
		pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=1)

		# Dense Layer
		pool2_flat = tf.reshape(pool2, [-1, 26 * 26 * number_of_filters])
		dense = tf.layers.dense(inputs=pool2_flat, units=128, activation=tf.nn.relu)
		dropout = tf.layers.dropout(
			inputs=dense, rate=0.4, training=mode == tf.estimator.ModeKeys.TRAIN
		)

		# Logits Layer
		logits = tf.layers.dense(inputs=dropout, units=10)

		predictions = {
			# Generate predictions (for PREDICT and EVAL mode)
			"classes": tf.argmax(input=logits, axis=1),
			# Add `softmax_tensor` to the graph. It is used for PREDICT and by the
			# `logging_hook`.
			"probabilities": tf.nn.softmax(logits, name="softmax_tensor")
		}

		accuracy_m, accuracy_op = tf.metrics.accuracy(
			labels=labels, predictions=predictions["classes"]
		)

		if mode == tf.estimator.ModeKeys.PREDICT:
			return tf.estimator.EstimatorSpec(mode=mode, predictions=predictions)

		# Calculate Loss (for both TRAIN and EVAL modes)
		onehot_labels = tf.one_hot(indices=tf.cast(labels, tf.int32), depth=10)
		loss = tf.losses.softmax_cross_entropy(
			onehot_labels=onehot_labels, logits=logits
		)

		# Configure the Training Op (for TRAIN mode)
		if mode == tf.estimator.ModeKeys.TRAIN:
			optimizer = tf.train.GradientDescentOptimizer(learning_rate=LEARNING_RATE)
			train_op = optimizer.minimize(
				loss=loss,
				global_step=tf.train.get_global_step()
			)
			return tf.estimator.EstimatorSpec(mode=mode, loss=loss, train_op=train_op)

		# Add evaluation metrics (for EVAL mode)
		eval_metric_ops = {
			"accuracy": tf.metrics.accuracy(
				labels=labels, predictions=predictions["classes"]
			)
		}

		return tf.estimator.EstimatorSpec(
			mode=mode, loss=loss, eval_metric_ops=eval_metric_ops
		)

def main(unused_argv):
	with tf.device('/cpu:0'):
	# with tf.device('/device:GPU:0'):
		# Load training and eval data
		mnist = tf.contrib.learn.datasets.load_dataset("mnist")
		train_data = mnist.train.images # Returns np.array
		train_labels = np.asarray(mnist.train.labels, dtype=np.int32)
		eval_data = mnist.test.images # Returns np.array
		eval_labels = np.asarray(mnist.test.labels, dtype=np.int32)

		# Create the Estimator
		mnist_classifier = tf.estimator.Estimator(
			model_fn=cnn_model_fn, model_dir="/tmp/mnist_convnet_model"
		)

		size_of_training_example_per_cycle = 500;
		number_of_training_examples = train_data.shape[0]
		number_of_total_runs = number_of_training_examples // size_of_training_example_per_cycle
		batch_size = 100
		number_of_epochs_per_cycle = 4
		number_of_steps = number_of_epochs_per_cycle * (size_of_training_example_per_cycle // batch_size)

		print("LEARNING RATE:")
		print(LEARNING_RATE)

		t0 = time.time()

		for i in range(0,number_of_total_runs):
			print("Step: {}".format(i))
			train_input_fn = tf.estimator.inputs.numpy_input_fn(
				x={"x": train_data[(size_of_training_example_per_cycle*i):(size_of_training_example_per_cycle*i) + size_of_training_example_per_cycle - 1]},
				y=train_labels[(size_of_training_example_per_cycle*i):(size_of_training_example_per_cycle*i) + size_of_training_example_per_cycle - 1],
				batch_size=batch_size,
				num_epochs=None,
				shuffle=True
			)

			mnist_classifier.train(
				input_fn=train_input_fn,
				steps=number_of_steps
			)

			# Evaluate the model and print results
			eval_input_fn = tf.estimator.inputs.numpy_input_fn(
				x={"x": eval_data},
				y=eval_labels,
				num_epochs=1,
				shuffle=False
			)

			eval_results = mnist_classifier.evaluate(input_fn=eval_input_fn)
			print(eval_results)

		t1 = time.time()

		print("Time to train: {:.2f}s".format(t1 - t0))


if __name__ == "__main__":
  tf.app.run()